// declare a variable that would be the initial balance
let balance = 100.00;

//create an empty array that would contain all the transactions
let history = [];

//create a function that would be triggered by the balance button, would make the label "answer_balance" visible and would hide the label "answer_history". Also, it sets the text for the balance label
document.getElementById("balance_btn").onclick = function get_balance() {
  document.getElementById('answer_history').style.display = "none";
  document.getElementById('answer_balance').style.display = "block";
  document.getElementById('answer_balance').innerHTML = "Your current balance is: " + "$" + balance;
};

//create a function that would be triggered by the deposit button and would have the purpose to make a dpeosit equal to the value written in the alert textbox. 
document.getElementById("deposit_btn").onclick = function make_deposit() {
  let deposit = parseFloat(prompt("How much would you like to deposit?"));
  // if the value is not a number or an empty string, give an error and close the alert (maybe the user changed their mind).
  if (isNaN(deposit) || deposit == "") {
    alert("Error! Please enter a number!");
    window.close();
  } 
  // else, the balance value will be a sum of the initial balance and the deposit made
  else {
    balance += deposit
    // add every new deposit to the history array with the '+' sign
    history.push('+$' + deposit);
     // show the new balance
  document.getElementById('answer_balance').style.display = "block";
  document.getElementById('answer_balance').innerHTML = "Your current balance is: " + "$" + balance;
  }
};

// create a function that would withdraw a value from the balance and display the new balance, similar to make_deposit function
document.getElementById("withdrawal_btn").onclick = function make_withdrawal() {
  let withdrawal = parseFloat(prompt("How much would you like to withdraw?"));
  if (isNaN(withdrawal) || withdrawal == "") {
    alert("Error! Please enter a number!");
    window.close();
  }
  else {
  balance -= withdrawal;
    // add every withdraw to the history array with the '-' sign
  history.push('- $' + withdrawal);
     // show the new balance
  document.getElementById('answer_balance').style.display = "block";
  document.getElementById('answer_balance').innerHTML = "Your current balance is: " + "$" + balance;
  }
};

// create a function that would show the history of all transactions made (deposit and withdraw) and would show the current balance
document.getElementById("history_btn").onclick = function get_history() {
// create a loop that would iterate through each transaction in the history array and display it
  document.getElementById('answer_history').innerHTML = "";
for(var i=0; i< history.length; i++) {
  document.getElementById('answer_transactions').style.display = "block";
  document.getElementById('answer_transactions').innerHTML = "Transactions: ";
  document.getElementById('answer_history').style.display = "block";
  document.getElementById('answer_history').innerHTML += history[i] + "<br/>";
}
};

// when exit button is clicked, an alert pops up for confirmation and exits the app (goes back tot the first page).
document.getElementById("exit_btn").onclick = function exit() {
  let confirm_exit = confirm("Are you sure you want to exit?");
  if (confirm_exit) {
     location.href = "https://codepen.io/dianet/full/mqaKWK";
  }
};


